# UbuntuTouch Pyton App Development Documentation and Examples

Documentation and example programs for learning Python app development on
Ubuntu Touch.

# Getting Started

* [Ubuntu Touch app development training, part 1](https://ubports.gitlab.io/marketing/education/ub-clickable-1/index.html)
* [vim-qml](https://vimawesome.com/plugin/vim-qml)

**Note**: It took me several days of poking around before I discovered the
above resource, which is both the most current and the best written
introduction that I've seen.  Ubports should make this easier to find on their
website, IMHO, if they want to help grow the app development community.

I use vim as my editor, and realized qml support would be really helpful. I am
an old, perpetual newbie, so despite years of using vim I haven't used plugins.
After trying several of them (and failing), the Pathogen instructions seemed
to have worked.

## Resources

* [Developer guides](https://docs.ubports.com/en/latest/appdev/guides/)
* [Ubports App Development](https://zlamalp.gitlab.io/2017/10/11/ubports-app-development/)
* [Quickstart: Python + Ubuntu Touch apps](https://aaron.place/blogs/UbPortsPython.html)
* [Python examples](https://forums.ubports.com/topic/5525/python-examples)
* [Developing Ubuntu Touch apps with Python using pyOtherSide](https://www.peterspython.com/en/blog/developing-ubuntu-touch-apps-with-python-using-pyotherside)
* [qml-tools plugin for vscodium](https://github.com/parasharrajat/qml-tools)

## Things I Need to Understand

* [What is LXD?](https://linuxcontainers.org/lxd/)
* [LXC vs Docker: Why Docker is Better in 2022](https://www.upguard.com/blog/docker-vs-lxc)
* [Qt Quick Local Storage QML Types](https://doc.qt.io/qt-5/qtquick-localstorage-qmlmodule.html)
